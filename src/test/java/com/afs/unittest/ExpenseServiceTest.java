package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project project = new Project(ProjectType.INTERNAL, "哈哈害");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);

        // then
        assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE, expenseCodeByProject);

    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project projectA = new Project(ProjectType.EXTERNAL, "Project A");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(projectA);

        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_A, expenseCodeByProject);

    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project projectB = new Project(ProjectType.EXTERNAL, "Project B");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(projectB);

        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_B, expenseCodeByProject);

    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project hahahai = new Project(ProjectType.EXTERNAL, "哈哈害");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(hahahai);

        // then
        assertEquals(ExpenseType.OTHER_EXPENSE, expenseCodeByProject);

    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        ExpenseService expenseService = new ExpenseService();

        // when
        Executable executable = () -> expenseService.getExpenseCodeByProject(new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "哈哈害"));

        // then
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, executable);
        assertEquals("You enter invalid project type", unexpectedProjectTypeException.getMessage());

    }
}